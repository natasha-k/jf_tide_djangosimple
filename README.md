# JF Tide Djangosimple

## Simple Django project for testing in Docker

We use docker-compose to build and manage the testapp service.

The following commands must be run from the project directory.

To start a detached service run:

    $ sudo docker-compose up -d

To test the service run:

    $ curl http://localhost:8000/testapp/testapp-endpoint

To view logs run:

    $ sudo docker-compose logs

To stop the service run:

    $ sudo docker-compose down

